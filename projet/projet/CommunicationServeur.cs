﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net; 
using System.Net.Sockets;


namespace BatailleCafe
{
    public static class CommunicationServeur
    {
        /*
         Connexion
         Role
             se connecter au serveur 
         Paramètres
            ip : adresse IP du serveur
            port : port du serveur
            connexionServeur : socket servant à communiquer avec le serveur
        Valeur de retour
            
        */
        public static void Connexion(string ip, int port, Socket connexionServeur)
        {
            try
            {
                Console.WriteLine("connexion au serveur");
                connexionServeur.Connect(ip, port);
            }
            catch (SocketException e)
            {
                Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
                throw;
            }
        }

        /*
         Deconnexion
         Role
             se deconnecter du serveur 
         Paramètres
            deconnexionServeur : socket servant à communiquer avec le serveur
        Valeur de retour
            
        */
        public static void Deconnexion(Socket deconnexionServeur)
        {
            try
            {
                Console.WriteLine("deconnexion du serveur");
                deconnexionServeur.Shutdown(SocketShutdown.Both);
                deconnexionServeur.Close();
            }
            catch (SocketException e)
            {
                Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
            }
        }

        /*
         ReceptionDonnee
         Role
             récuperer les données envoyées par le serveur 
         Paramètres
            receptionServeur : socket servant à communiquer avec le serveur
            data : tableau de byte dans lequels sera stocké les données reçu
        Valeur de retour
            
        */
        public static void ReceptionDonnee(Socket receptionServeur, byte[] data)
        {
            if (receptionServeur.Connected)
            {
                try
                {
                    receptionServeur.Receive(data);
                }
                catch (SocketException e)
                {
                    Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
                }
            }
            else
            {
                Console.WriteLine("Socket non connecté");
            }

        }

        /*
        EnvoiDonnee
        Role
            envoyé des données au serveur 
        Paramètres
           receptionServeur : socket servant à communiquer avec le serveur
           coup : tableau de byte contenant le coup du joueur
       Valeur de retour

       */
        public static void EnvoiDonnee(Socket receptionServeur, byte[] coup)
        {
            try
            {
                receptionServeur.Send(coup);
            }
            catch(SocketException e)
            {
                Console.WriteLine("{0} Error code: {1}.", e.Message, e.ErrorCode);
            }
        }

        /*
        EnvoieGraine
        Role
            envoyé les cordonnées de la parcelles ou on plante notre graine
        Paramètres

       Valeur de retour

       */
        public static void EnvoieGraine(Socket socket, Coordonnees coordonneeGraine)
        {
            string xCoordonnee = coordonneeGraine.getX().ToString();
            string yCoordonnee = coordonneeGraine.getY().ToString();
            string trameCoordonnee = string.Concat("A:", xCoordonnee, yCoordonnee);
            byte[] maGraine = Encoding.UTF8.GetBytes(trameCoordonnee);
            socket.Send(maGraine);
        }

        /*
        ReceptionCoupServeur
        Role
            reçoi la parcelle ou le serveur à planter la graine
        Paramètres

       Valeur de retour

      */
        public static Coordonnees ReceptionCoupServeur(Socket socket)
        {
            byte[] coupServeur = new byte[4];
            ReceptionDonnee(socket, coupServeur);
            int xCoordonnee = -1;
            int yCoordonnee = -1;
            if (!(String.Equals(Encoding.UTF8.GetString(coupServeur), "FINI")))
            {
                try
                {
                    xCoordonnee = Int32.Parse(Encoding.UTF8.GetString(new[] { coupServeur[2] }));
                    yCoordonnee = Int32.Parse(Encoding.UTF8.GetString(new[] { coupServeur[3] }));
                }
                catch (FormatException)
                {
                    Console.WriteLine("Unable to parse coordonnées");
                }
            }
            return new Coordonnees(xCoordonnee, yCoordonnee);
        }


        /*
        JouerUnCoup
        Role
            joue un coup
        Paramètres
            socket socket communiquant avec le serveur
            coordonneeGraine cordinnee de plantage de la graine 
            
       Valeur de retour
            true si la partie est fini false sinon
       */
        public static bool JouerUnCoup(Socket socket, Coordonnees coordonneeGraine, Jeu jeu)
        {
            bool partieFinie = false;
            //Envoie du coup
            EnvoieGraine(socket, coordonneeGraine);

            //Recupération si Valide ou non
            byte[] coupValide = new byte[4];
            ReceptionDonnee(socket, coupValide);
            //Si VALI : Stocker
            //Si INVA : Ne Pas Stocker
            if (String.Equals(Encoding.UTF8.GetString(coupValide), "VALI"))
            {
                jeu.MajJeu(coordonneeGraine, "Blanche");
            }

            //Recupération du jeu serveur
            Coordonnees coupServeur;
            coupServeur = ReceptionCoupServeur(socket);
            if (coupServeur.getX() == -1 && coupServeur.getY() == -1)//si -1 -1 => serveur envoie FINI
            {
                return true;
            }
            else
            {
                //Stocker coord coupServeur
                jeu.MajJeu(coupServeur, "Noire");
            }

            //Recuperation ENCO/FINI
            byte[] fin = new byte[4];
            ReceptionDonnee(socket, fin);
            if (String.Equals(Encoding.UTF8.GetString(fin), "FINI"))
            {
                partieFinie = true;
            }
            return partieFinie;

        }

        /*
        ReceptionScore
        Role
            reçois et affiche le score final
        Paramètres
            socket socket communiquant avec le serveur
       Valeur de retour
           
       */
        public static void ReceptionScore(Socket socket)
        {
            byte[] score = new byte[7];
            ReceptionDonnee(socket, score);
            Console.WriteLine(Encoding.UTF8.GetString(score));
            String[] tabScore = (Encoding.UTF8.GetString(score)).Split(':');
            Console.WriteLine("Score Joueur: {0}", tabScore[1]);
            Console.WriteLine("Score Serveur: {0}", tabScore[2]);
        }


    }
}