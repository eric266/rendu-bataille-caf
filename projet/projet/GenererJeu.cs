﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe
{
    class GenererJeu
    {
        private char[,] carte;

        /*
        GenererJeu
        Role
            constructeur de classe 
        Paramètres
           carte : carte de l'ile
        Valeur de retour

       */
        public GenererJeu(char[,] carte)
        {
            this.carte = carte;
        }

        /*
        AjoutUniteeAParcelle
        Role
            ajoute les unitées à leur parcelles 
        Paramètres
           parcelles : liste des parcelles a verifier
           numParcelles : numero de la parcelle
           coordonneeX : coordonnée x de l'unitée
           coordonneeY : coordonnée y de l'unitée
        Valeur de retour

       */
        private void AjoutUniteeAParcelle(List<Parcelle> parcelles, char numParcelle, int coordonneeX, int coordonneeY)
        {
            bool trouver = false;
            foreach (Parcelle parcelle in parcelles)
            {
                if (parcelle.getNom() == numParcelle.ToString())
                {
                    trouver = true;
                    parcelle.AjoutUnite(new Unite(new Coordonnees(coordonneeX, coordonneeY), null,null));
                }
            }
            if (!trouver)
            {
                Parcelle nouvelleParcelle = new Parcelle(null,numParcelle.ToString());
                nouvelleParcelle.AjoutUnite(new Unite(new Coordonnees(coordonneeX, coordonneeY), null, null));
                parcelles.Add(nouvelleParcelle);
            }
        }

        public void AddFrontiereToUnites(Ile ilot)
        {
            foreach (Parcelle parcelle in ilot.getTouteParcelle())
            {
                foreach (Unite unite in parcelle.getTouteUnite())
                {
                    List<Unite> uniteVoisin = unite.getVoisins(ilot);
                    for (int indexVoisin = 0; indexVoisin < 4; indexVoisin++)
                    {
                        if (!(uniteVoisin[indexVoisin] is null))
                        {
                            if (!ReferenceEquals(parcelle, uniteVoisin[indexVoisin].getParcelle(ilot)))
                            {
                                if (indexVoisin == 0)
                                    unite.PushFrontiere("N");
                                if (indexVoisin == 1)
                                    unite.PushFrontiere("S");
                                if (indexVoisin == 2)
                                    unite.PushFrontiere("E");
                                if (indexVoisin == 3)
                                    unite.PushFrontiere("W");
                            }
                        }
                    }
                }
            }
        }

        public Jeu GenererPartie()
        {
            List<Parcelle> parcelles = new List<Parcelle>();
            for (int indexLigne = 0; indexLigne <= 9; indexLigne++)
            {
                for (int indexColonne = 0; indexColonne <= 9; indexColonne++)
                {
                    this.AjoutUniteeAParcelle(parcelles, this.carte[indexLigne, indexColonne], indexLigne, indexColonne);
                }
            }

            Ile ilot = new Ile(parcelles);

            this.AddFrontiereToUnites(ilot);

            return new Jeu(ilot);
        }







        /*public static bool operator ==(Unite unite1, Unite unite2)
        {
            if (unite1 is null || unite2 is null)
            {
                return false;
            }
            return unite1.getCoordonnees() == unite2.getCoordonnees() && unite1.getFrontiere() == unite2.getFrontiere() && unite1.getGraine() == unite2.getGraine();
        }

        public static bool operator !=(Unite unite1, Unite unite2)
        {
            return !(unite1 == unite2);
        }

        
        public override bool Equals(object obj)
        {
            return obj is Unite && this == (Unite)obj;
        }*/

    }
}
