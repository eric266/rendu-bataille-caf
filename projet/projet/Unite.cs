﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe
{
    public class Unite
    {
        private Coordonnees coordonnees;
        private String graine; //"Blanche" ou "Noire"
        private String frontiere; // exemple: NSEO entourée de frontière N frontière au nord NE frontière au nord et a l'est

        /*
         Unite
         Role
             constructeur de classe 
         Paramètres
            coordonnees : coordonnées de l'unité
            graine : graine de l'unité
            frontiere : frontière de l'unité
         Valeur de retour
            
        */
        public Unite(Coordonnees coordonnees, String frontiere, String graine)
        {
            this.coordonnees = (coordonnees is null) ? new Coordonnees(-1, -1) : coordonnees;
            this.frontiere = (frontiere is null) ? "" : frontiere;
            PlanterGraine(graine);
        }

        /*
         getCoordonnees
         Role
             recuperer les coordonnées de l'unitée
         Paramètres
            
         Valeur de retour
             les coordonnées de l'unitée
        */
        public Coordonnees getCoordonnees()
        {
            return this.coordonnees;
        }

        /*
         UniteeOccupee
         Role
             savoir si une graine à été palnter dans l'unitée 
         Paramètres
            
         Valeur de retour
             si la longueur de graine est > à 0 retourne vrai
        */
        public Boolean UniteeOccupee()
        {
            if (this.graine.Length != 0)
                return true;
            else
                return false;
        }


        /*
         getGraine
         Role
             récuperer la valeur de graine de l'unitée
         Paramètres

         Valeur de retour
             la valeur de graine de l'unitée
        */
        public String getGraine()
        {
            return this.graine;
        }

        /*
         PossedeGraine
         Role
             determine si une graine est à l'endroit vérifié
         Paramètres
            
         Valeur de retour
            true si la grine est la false sinon
        */
        public Boolean PossedeGraine()
        {
            if (this.graine == null || this.graine.Length == 0)
                return false;
            else
                return true;
        }


        /*
         PlanterGraine
         Role
             planter une graine 
         Paramètres
            graine : graine à planter
         Valeur de retour
            
        */
        public void PlanterGraine(String graine)
        {
            if (!this.PossedeGraine())
                if (graine != null)
            {
                if (graine == "Blanche" || graine == "Noire")
                    this.graine = graine;
                else
                    this.graine = "";
            }
            else
            {
                this.graine = "";
            }

        }

        /*
         PossedeFrontiere
         Role
             verifi si l'unité possède une frontière 
         Paramètres

         Valeur de retour
             vrai si l'unité possède une frontière  sinon faux
        */
        public Boolean PossedeFrontiere()
        {
            if (this.frontiere.Length != 0)
                return true;
            else
                return false;
        }

        /*
         getFrontiere
         Role
             récupere la valeur de forntière de l'unité
         Paramètres

         Valeur de retour
             a valeur de forntière de l'unité
        */
        public String getFrontiere()
        {
            return this.frontiere;
        }

        /*
        MemeCoordonnees
        Role
            verifi si une unitée a les même coordonnée q'une autre
        Paramètres
            autreUnitee : unitée à comparer
        Valeur de retour
            vrai si coordonnées égals sinon faux
       */
        public Boolean MemeCoordonnees(Unite autreUnitee)
        {
            if (this.coordonnees == autreUnitee.coordonnees)
                return true;
            else
                return false;
        }

        /*
        getParcelle
        Role
            verifi a quelle parcelle appartient l'unitée
        Paramètres
           
        Valeur de retour
           
       */
        public Parcelle getParcelle(Ile ilot)
        {
            foreach (Parcelle parcelle in ilot.getTouteParcelle())
            {
                foreach (Unite unite in parcelle.getTouteUnite())
                {
                    if (this == unite)
                    {
                        return parcelle;
                    }
                }
            }
            return null;
        }

        public List<Unite> getVoisins(Ile ilot)
        {
            List<Unite> allVoisin = new List<Unite>();
            Unite voisinNord = null;
            if (!((voisinNord = getVoisinNord(ilot)) is null))
            {
                allVoisin.Add(voisinNord);
            }
            else
            {
                allVoisin.Add(null);
            }
            Unite voisinSud = null;
            if (!((voisinSud = getVoisinSud(ilot)) is null))
            {
                allVoisin.Add(voisinSud);
            }
            else
            {
                allVoisin.Add(null);
            }
            Unite voisinEst = null;
            if (!((voisinEst = getVoisinEst(ilot)) is null))
            {
                allVoisin.Add(voisinEst);
            }
            else
            {
                allVoisin.Add(null);
            }
            Unite voisinOuest = null;
            if (!((voisinOuest = getVoisinOuest(ilot)) is null))
            {
                allVoisin.Add(voisinOuest);
            }
            else
            {
                allVoisin.Add(null);
            }
            return allVoisin;
        }

        public Unite getVoisinNord(Ile ilot)
        {
            if (getCoordonnees().getY() != 0)
            {
                Unite voisinNord = null;
                foreach (Parcelle parcelle in ilot.getTouteParcelle())
                {
                    foreach (Unite unite in parcelle.getTouteUnite())
                    {
                        if (unite.getCoordonnees().getY() == getCoordonnees().getY() && unite.getCoordonnees().getX() == getCoordonnees().getX() - 1)
                            voisinNord = unite;
                    }
                }
                return voisinNord;
            }
            else
                return null;
        }

        public Unite getVoisinSud(Ile ilot)
        {
            Unite voisinSud = null;
            foreach (Parcelle parcelle in ilot.getTouteParcelle())
            {
                foreach (Unite unite in parcelle.getTouteUnite())
                {
                    if (unite.getCoordonnees().getY() == getCoordonnees().getY() && unite.getCoordonnees().getX() == getCoordonnees().getX() + 1)
                        voisinSud = unite;
                }
            }
            return voisinSud;
        }

        public Unite getVoisinEst(Ile ilot)
        {
            Unite voisinEst = null;
            foreach (Parcelle parcelle in ilot.getTouteParcelle())
            {
                foreach (Unite unite in parcelle.getTouteUnite())
                {
                    if (unite.getCoordonnees().getY() == getCoordonnees().getY() + 1 && unite.getCoordonnees().getX() == getCoordonnees().getX())
                        voisinEst = unite;
                }
            }
            return voisinEst;
        }

        public Unite getVoisinOuest(Ile ilot)
        {
            Unite voisinOuest = null;
            foreach (Parcelle parcelle in ilot.getTouteParcelle())
            {
                foreach (Unite unite in parcelle.getTouteUnite())
                {
                    if (unite.getCoordonnees().getY() == getCoordonnees().getY() - 1 && unite.getCoordonnees().getX() == getCoordonnees().getX())
                        voisinOuest = unite;
                }
            }
            return voisinOuest;
        }


        public Boolean AFrontiere()
        {
            if (this.frontiere.Length != 0)
                return true;
            else
                return false;
        }

        public String GetFrontiere()
        {
            return this.frontiere;
        }

        public void PushFrontiere(string frontiere)
        {
            this.frontiere += frontiere;
        }




        public static bool operator ==(Unite unite1, Unite unite2)
        {
            if (unite1 is null || unite2 is null)
            {
                return false;
            }
            return unite1.getCoordonnees() == unite2.getCoordonnees() && unite1.getFrontiere() == unite2.getFrontiere() && unite1.getGraine() == unite2.getGraine();
        }

        public static bool operator !=(Unite unite1, Unite unite2)
        {
            return !(unite1 == unite2);
        }

        public override bool Equals(object obj)
        {
            return obj is Unite && this == (Unite)obj;
        }

    }
}
