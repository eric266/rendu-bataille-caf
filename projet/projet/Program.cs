﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace BatailleCafe
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] tableau = new byte[300];
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //CommunicationServeur.Connexion("51.91.120.237", 1212, socket); //serveur reseau "normale"
            //CommunicationServeur.Connexion("172.16.0.88", 1212, socket); //serveur reseau IUT;
            //CommunicationServeur.Connexion("51.91.120.237", 1213, socket); // serveur graines "normale"
            //CommunicationServeur.Connexion("172.16.0.88", 1213, socket); // serveur graines IUT
            CommunicationServeur.Connexion("127.0.0.1", 1213, socket);
            CommunicationServeur.ReceptionDonnee(socket, tableau);
           

            String trame = Encoding.UTF8.GetString(tableau);
            Console.WriteLine(trame);
            Console.ReadKey();


            int[,] tableauTrame = new int[10,10];
            Decodage.SeparationTrame(trame, tableauTrame);

            char[,] carte = new char[10, 10];
            Decodage.DecoderTrame(tableauTrame, carte);

            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    Console.Write("{0} ", carte[ligne, colonne]);
                }
                Console.WriteLine();
            }

            GenererJeu generateur = new GenererJeu(carte);

            Jeu jeu = generateur.GenererPartie();

           

            Coordonnees coordonnee;

            do
            {
                coordonnee = jeu.getIa().DeterminerCoup(jeu.getIlot());
            } while (!CommunicationServeur.JouerUnCoup(socket, coordonnee, jeu));

            CommunicationServeur.ReceptionScore(socket);


            CommunicationServeur.Deconnexion(socket);

            Console.ReadKey();

        }
    }
}
