﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe
{
    public class Parcelle
    {
        List<Unite> ToutesUnite;
        String nom;

        public Parcelle(String name)
        {
            this.nom = name;
            this.ToutesUnite = new List<Unite>();
        }

        /*
        Parcelle
         Role
             constructeur de classe
        Paramètres
           ToutesUnite : toutes les unitées constituant la parcelle
           nom : nom de la parcelle
       */
        public Parcelle(List<Unite> ToutesUnite, String nom)
        {
            Coordonnees coodrdoneeDefaut =new Coordonnees(-1, -1);
            List<Unite> ToutesUniteDefaut = new List<Unite>();
            ToutesUniteDefaut.Add(new Unite(coodrdoneeDefaut, "",""));
            this.ToutesUnite = (ToutesUnite is null) ? ToutesUniteDefaut : ToutesUnite;
            this.nom = (nom is null) ? "" : nom;
        }

        //constructeur par recopie
        public Parcelle(Parcelle autreParcelle) : this(autreParcelle.ToutesUnite, autreParcelle.nom) { }

        /*
         getNombrebGraineBlanche
         Role
             récuperer le nombre de graines blanches dans la parcelle 
         Paramètres

         Valeur de retour
             nombrebGraineBlanche : le nombre de graines blanches dans la parcelle
        */
        public int getNombrebGraineBlanche()
        {
            int nombrebGraineBlanche = 0;
            foreach (Unite unite in ToutesUnite)
                if (unite.UniteeOccupee())
                    if (unite.getGraine() == "Blanche")
                        nombrebGraineBlanche++;
            return nombrebGraineBlanche;
        }

        /*
         getNombrebGraineNoire
         Role
             récuperer le nombre de graines noire dans la parcelle 
         Paramètres

         Valeur de retour
             nombrebGraineNoire : le nombre de graines noire dans la parcelle
        */
        public int getNombrebGraineNoire()
        {
            int nombrebGraineNoire = 0;
            foreach (Unite unite in ToutesUnite)
                if (unite.UniteeOccupee())
                    if (unite.getGraine() == "Noire")
                        nombrebGraineNoire++;
            return nombrebGraineNoire;
        }

        /*
         getNombrebGraine
         Role
             récuperer le nombre de graines dans la parcelle 
         Paramètres

         Valeur de retour
             nombrebGraineNoire : le nombre de graines dans la parcelle
        */
        public int getNombrebGraine()
        {
            return getNombrebGraineNoire() + getNombrebGraineBlanche();
        }

        /*
         getNombrebGraine
         Role
             récuperer le nombre d'unitées dans la parcelle 
         Paramètres

         Valeur de retour
             nombre d'unitées dans la parcelle
        */
        public int getNombreUnite()
        {
            return ToutesUnite.Count;
        }

        /*
         getNombrebGraine
         Role
             récuperer l'attribut ToutesUnite de la parcelle
         Paramètres

         Valeur de retour
             l'attribut ToutesUnite de la parcelle
        */
        public List<Unite> getTouteUnite()
        {
            return ToutesUnite;
        }

        /*
        getNombrebGraine
        Role
            récuperer l'attribut nom de la parcelle
        Paramètres

        Valeur de retour
            l'attribut nom de la parcelle
       */
        public String getNom()
        {
            return nom;
        }

        /*
        AjoutUnite
        Role
            ajoute une unité à une parcelle
        Paramètres
            unite : unitée à ajouter
        Valeur de retour
            
       */
        public void AjoutUnite(Unite unite)
        {
            this.ToutesUnite.Add(unite);
        }


    }
}
