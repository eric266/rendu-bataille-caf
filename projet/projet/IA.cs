﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe
{
    public class IA
    {
        private Unite dernierreGraineUnite;

        /*
         IA
         Role
             constructeur 
         Paramètres

        valeur de retour

        */
        public IA()
        {
            dernierreGraineUnite = null;
        }

        /*
         setDernierreGraineCoordonnees
         Role
             enregistrer les coedoonées de la dernière graine posée 
         Paramètres

        valeur de retour

        */
        public void setDernierreGraineCoordonnees(Unite unite)
        {
            dernierreGraineUnite = unite;
        }

        /*
         UniteValide
         Role
             retourne toutes les unites où il est possible de jouer à ce tour de jeu en fonction de la position de la dernière graine posée
         Paramètres
            unite unitée vérifié pour la validité du tour de jeu
            ilot carte du jeu
        valeur de retour
            bool
        */
        public bool UniteValide(Unite unite, Ile ilot)
        {

            if (dernierreGraineUnite is null)
                return true;

            return ((unite.getCoordonnees().getX() == dernierreGraineUnite.getCoordonnees().getX()) || unite.getCoordonnees().getY() == dernierreGraineUnite.getCoordonnees().getY()) && !unite.UniteeOccupee() && !ReferenceEquals(dernierreGraineUnite.getParcelle(ilot), unite.getParcelle(ilot));
        }

        public Coordonnees DeterminerCoup(Ile ilot)
        {
            Coordonnees meilleurecoup = null;
            float valmeilleurecoup = 0;

            foreach (Parcelle parcelle in ilot.getTouteParcelle())
            {
                if (parcelle.getNom() != "M" && parcelle.getNom() != "F")
                {
                    foreach (Unite unite in parcelle.getTouteUnite())
                    {
                        if (UniteValide(unite, ilot))
                        {
                            float poids = DeterminerPrioriteeUnite(unite, ilot);
                            if (poids >= valmeilleurecoup)
                            {
                                meilleurecoup = unite.getCoordonnees();
                                valmeilleurecoup = poids;
                                
                            };
                        }
                    }
                }
            }

            if(ilot.getNombreGraineBlanche()<=0)
            {
                Coordonnees defaut = new Coordonnees(1, 1);
                meilleurecoup = defaut;
            }
            return meilleurecoup;
        }

        public float DeterminerPrioriteeUnite(Unite unite, Ile ilot)
        {
            float poids = 0;

            // s'il y a égalité sur une parcelle il faut prendre l'avantage
            poids += Egalitee(unite, ilot);

            // jouer le plus au centre prossible
            poids += Math.Abs(unite.getCoordonnees().getX() - 5) / 2;
            poids += Math.Abs(unite.getCoordonnees().getY() - 5) / 2;

            // jouer parcelle impair puis parcelles les plus grande
            poids += TailleParcelle(unite, ilot);

            // arreter de jouer sur une parcelle quand le monopole de cette parcelle est déjà pris
            poids += ParcelleMonopoliser(unite, ilot);

            // sécuriser ou bloquer un monopole si possible
            poids += SecuriserMonopole(unite, ilot);

            // avoir le plus grand alignement de graine
            poids += Alignement(unite, ilot);


            return poids;
        }

        /*
         Egalitee
         Role
             ajoute un poids en cas d'égalité sur une parcelle
         Paramètres
            unite unitée vérifié pour la validité du tour de jeu
            ilot carte du jeu
        valeur de retour
            int
        */
        public int Egalitee(Unite unite, Ile ilot)
        {
            int poids = 0;
            float graineBlancche = unite.getParcelle(ilot).getNombrebGraineBlanche();
            float graineNoire = unite.getParcelle(ilot).getNombrebGraineNoire();
            if (( (graineNoire + 1) / (graineBlancche + 1) ) == 1)
            {
                poids += 10;
            };
            return poids;
        }

        /*
        TailleParcelle
        Role
            ajoute un poids en fonction de la taille de la parcelle
        Paramètres
           unite unitée vérifié pour la validité du tour de jeu
           ilot carte du jeu
       valeur de retour
           float
       */
        public float TailleParcelle(Unite unite, Ile ilot)
        {
            float poids = 0;
            if (unite.getParcelle(ilot).getNombreUnite() == 3)
            {
                poids += 1 / 2;
            }
            else
            {
                poids += (float)unite.getParcelle(ilot).getNombreUnite() / 18;
            }
            return poids;
        }

        /*
       ParcelleMonopoliser
       Role
           enlève un poids en fonction du monopole de la parcelle par le joueur
       Paramètres
          unite unitée vérifié pour la validité du tour de jeu
          ilot carte du jeu
      valeur de retour
          int
      */
        public int ParcelleMonopoliser(Unite unite, Ile ilot)
        {
            int poids = 0;
            float graineBlancche = unite.getParcelle(ilot).getNombrebGraineBlanche();
            float graineNoire = unite.getParcelle(ilot).getNombrebGraineNoire();
            float unitee = unite.getParcelle(ilot).getNombreUnite();
            if ((graineBlancche / unitee) > 0.5 || (graineNoire / unitee) > 0.5)
            {
                poids -= 10;
            }
            return poids;
        }

        /*
       SecuriserMonopole
       Role
           ajoute un poids en fonction du monopole sécurisable par le joueur
       Paramètres
          unite unitée vérifié pour la validité du tour de jeu
          ilot carte du jeu
      valeur de retour
          int
      */
        public int SecuriserMonopole(Unite unite, Ile ilot)
        {
            float graineBlancche = unite.getParcelle(ilot).getNombrebGraineBlanche();
            float graineNoire = unite.getParcelle(ilot).getNombrebGraineNoire();
            float unitee = unite.getParcelle(ilot).getNombreUnite();
            int poids = 0;
            if ((graineBlancche / unitee) == 0.5 || (graineNoire / unitee) == 0.5)
            {
                poids += 10;
            }
            return poids;
        }

        /*
       Alignement
       Role
           ajoute un poids en fonction du monopole sécurisable par le joueur
       Paramètres
          unite unitée vérifié pour la validité du tour de jeu
          ilot carte du jeu
      valeur de retour
          int
      */
        public int Alignement(Unite unite, Ile ilot)
        {
            int poids = 0;
            int plusLongBlancX = 0;
            int plusLongBlancXN = -1;
            int plusLongNoirX = 0;
            int plusLongNoirXN = -1;
            for (int X = 0; X < 10; X++)
            {
                int nbgraineblanche = 0;
                int nbgrainenoire = 0;
                for (int Y = 0; Y < 10; Y++)
                {

                    string graine = ilot.getUniteeA(new Coordonnees(X, Y)).getGraine();
                    if (graine == "Noire")
                    {
                        nbgrainenoire += 1;
                    }
                    if (graine == "Blanche")
                    {
                        nbgraineblanche += 1;
                    }
                }
                if (nbgraineblanche > plusLongBlancX)
                {
                    plusLongBlancX = nbgraineblanche;
                    plusLongBlancXN = X;
                }
                if (nbgrainenoire > plusLongNoirX)
                {
                    plusLongNoirX = nbgrainenoire;
                    plusLongNoirXN = X;
                }
            }

            int plusLongBlancY = 0;
            int plusLongBlancYN = -1;
            int plusLongNoireY = 0;
            int plusLongNoireYN = -1;
            for (int Y = 0; Y < 10; Y++)
            {
                int nbgraineblanche = 0;
                int nbgrainenoire = 0;
                for (int X = 0; X < 10; X++)
                {

                    string graine = ilot.getUniteeA(new Coordonnees(X, Y)).getGraine();
                    if (graine == "Noire")
                    {
                        nbgrainenoire += 1;
                    }
                    if (graine == "Blanche")
                    {
                        nbgraineblanche += 1;
                    }
                }
                if (nbgraineblanche > plusLongBlancY)
                {
                    plusLongBlancY = nbgraineblanche;
                    plusLongBlancYN = Y;
                }
                if (nbgrainenoire > plusLongNoireY)
                {
                    plusLongNoireY = nbgrainenoire;
                    plusLongNoireYN = Y;
                }
            }

            if ((plusLongBlancX <= plusLongNoirX || plusLongBlancX <= plusLongNoireY) && (plusLongBlancY <= plusLongNoireY || plusLongBlancY <= plusLongNoirX))
            {
                if (plusLongBlancX <= plusLongBlancY)
                {
                    if (unite.getCoordonnees().getY() == plusLongBlancYN)
                    { 
                        poids += 2;
                    }
                }
                else
                {
                    if (unite.getCoordonnees().getX() == plusLongBlancXN) 
                    { 
                        poids += 2;
                    }
                }
            }
            return poids;
        }






    }
}
