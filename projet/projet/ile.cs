﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe
{
    public class Ile
    {
        private List<Parcelle> TouteParcelle;

        /*
        Ile
        Role
            constructeur de classe 
        Paramètres
           TouteParcelle : liste des parcelles
        Valeur de retour

       */
        public Ile(List<Parcelle> TouteParcelle)
        {
            List<Parcelle> defautTouteParcelle = new List<Parcelle>();
            defautTouteParcelle.Add(new Parcelle(new List<Unite>(), "error"));
            this.TouteParcelle = (TouteParcelle is null) ? defautTouteParcelle : TouteParcelle;
        }

        // constructeur par recopie
        public Ile(Ile autreIle) : this(autreIle.getTouteParcelle()) { }

       /*
       getTouteParcelle
       Role
           recuper l'attribut TouteParcelle 
       Paramètres

       Valeur de retour
           l'attribut TouteParcelle
       */
        public List<Parcelle> getTouteParcelle()
        {
            return TouteParcelle;
        }

       /*
       getUniteeA
       Role
           recuper une unitée selon ces coordonnées
       Paramètres
           coordonnees : coordonnées de l'unitée
       Valeur de retour
           uniteARetourner : unitée récupérer
       */
        public Unite getUniteeA(Coordonnees coordonnees)
        {
            Unite uniteARetourner = null;
            foreach (Parcelle parcelle in TouteParcelle)
            {
                foreach (Unite unite in parcelle.getTouteUnite())
                {
                    if (unite.getCoordonnees() == coordonnees)
                        uniteARetourner = unite;
                }
            }
            return uniteARetourner;
        }

      /*
      getNombreGraineBlanche
      Role
          recuperer le nombre de graine blanche sur l'ile
      Paramètres
          
      Valeur de retour
          nombreGraineBlanche : nombre de graine blanche sur l'ile
      */
        public int getNombreGraineBlanche()
        {
            int nombreGraineBlanche = 0;
            foreach (Parcelle parcelle in TouteParcelle)
            {
                nombreGraineBlanche = nombreGraineBlanche + parcelle.getNombrebGraineBlanche();
            }
            return nombreGraineBlanche;
        }

        /*
        getNombreGraineBlanche
        Role
            recuperer le nombre de graine noire sur l'ile
        Paramètres

        Valeur de retour
            nombreGraineNoire : nombre de graine noire sur l'ile
        */
        public int getNombreGraineNoire()
        {
            int nombreGraineNoire = 0;
            foreach (Parcelle parcelle in TouteParcelle)
            {
                nombreGraineNoire = nombreGraineNoire + parcelle.getNombrebGraineBlanche();
            }
            return nombreGraineNoire;
        }

        /*
        getNombreGraine
        Role
            recuperer le nombre de graine sur l'ile
        Paramètres

        Valeur de retour
            nombreGraineNoire : nombre de graine sur l'ile
        */
        public int getNombreGraine()
        {
            return getNombreGraineNoire() + getNombreGraineBlanche();
        }

        /*
        getParcelle
        Role
           recuperer une parcelle de l'ile
        Paramètres
           nom : nom de la parcelle
        Valeur de retour
           parcelleARetourner : parcelle de l'ile récuperer
       */
        public Parcelle getParcelle(String nom)
        {
            Parcelle parcelleARetourner = null;
            foreach (Parcelle parcelle in TouteParcelle)
            {
                if (parcelle.getNom() == nom)
                    parcelleARetourner = parcelle;
            }
            return parcelleARetourner;
        }


    }
}
