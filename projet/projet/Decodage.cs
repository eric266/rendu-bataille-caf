﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace BatailleCafe
{
    class Decodage
    {
        /*
         SeparationTrame
         Role
             supprime les caratères : et | et convertit la trame en un tableau
             d'entier à deux dimensions
         Paramètres
            trame : données reçues du serveurs
            tableauEntier : stocke la tame dans un tableau à deux dimensions
        Valeur de retour
            tableauEntier stocke la tame dans un tableau à deux dimensions
        */
        public static void SeparationTrame(string trame, int[,] tableauEntier)
        {
            String[] lignes = trame.Split('|');
            String[][] tableauString = new String[lignes.Length][];
            for (int index = 0; index < lignes.Length; index++)
            {
                tableauString[index] = lignes[index].Split(':');
            }

            for (int colonne = 0; colonne < 10; colonne++)
            {
                for (int ligne = 0; ligne < 10; ligne++)
                {
                    tableauEntier[colonne, ligne] = int.Parse(tableauString[colonne][ligne]);
                }
            }
        }

        /*
         DecoderTerrain
         Role
             decode le terrain selon trois type de base Terre , Mer, Foret
         Paramètres
            valeur : valeur du tableau d'entier créer avec SeparationTrame
        Valeur de retour
            lettre : type de terrain selon la valeur passer en paramètres
        */
        public static char DecoderTerrain(int valeur)
        {
            char lettre = 'T'; //terre
            if ((valeur - 64) >= 0)
            {
                valeur = valeur - 64;
                lettre = 'M';//mer
            }
            if ((valeur - 32) >= 0)
            {
                lettre = 'F';//foret
            }
            return lettre;
        }

        /*
         DecoderFrontiere
         Role
             decode les frontières d'une valeur du tableau (Nord,Ouest,Sud,Est)
         Paramètres
            valeur : valeur du tableau d'entier à decoder
            frontiere : tableau donnant les frontières d'une valeur
            exemple {true, true, true, false} la valeur à une frontière
            au Nord, a l'Ouest et au Sud
        Valeur de retour
            frontiere : tableau donnant les frontières d'une valeur
        */
        public static void DecoderFrontiere(int valeur, ref bool[] frontiere)
        {
            frontiere = new bool[4] { false, false, false, false };//[Nord,Ouest,Sud,Est]
            if ((valeur - 64) >= 0) { valeur = valeur - 64; }
            if ((valeur - 32) >= 0) { valeur = valeur - 32; }
            if (valeur - 8 >= 0)
            {
                valeur = valeur - 8;
                frontiere[3] = true;
            }//Est (2^4)
            if (valeur - 4 >= 0)
            {
                valeur = valeur - 4;
                frontiere[2] = true;
            }//Sud (2^3)
            if (valeur - 2 >= 0)
            {
                valeur = valeur - 2;
                frontiere[1] = true;
            }//Ouest (2^2)
            if (valeur - 1 >= 0)
            {
                valeur = valeur - 1;
                frontiere[0] = true;
            }//Nord (2^0)

        }

        /*
        DecoderParcelle
        Role
            decode les parcelles de la carte 
        Paramètres
           lettreActuelle : Plus grande lettre utilisée pour identifier les parcelles
           carte : Tableau de caractère indiquant les parcelles Foret/Mer/Terre
           ligne : identificateur de la ligne décoder
           colonne : identificateur de la colonne décoder
           frontiere : Tableau de Booléen indiquant si l'unité à une frontière 
           trame : Tableau de d'entier (trame reçu du serveur)
       Valeur de retour
           frontiere : tableau donnant les frontières d'une valeur
       */
        public static void DecoderParcelle(ref int lettreActuelle, char[,] carte, int ligne, int colonne,
                bool[] frontiere, int[,] trame)
        {
            if (carte[ligne, colonne] == 'T')
            {
                if (frontiere[0] && frontiere[1] && !(frontiere[3]))
                {
                    bool[] tabfrontieredroite = new bool[4];
                    DecoderFrontiere(trame[ligne, colonne + 1], ref tabfrontieredroite);

                    if (!(tabfrontieredroite[0]))
                    {
                        carte[ligne, colonne] = carte[ligne - 1, colonne + 1];
                    }
                    else
                    {
                        carte[ligne, colonne] = Convert.ToChar(lettreActuelle + 1);
                        lettreActuelle = carte[ligne, colonne];
                    }
                }
                if (frontiere[0] && frontiere[1] && frontiere[3])
                {
                    carte[ligne, colonne] = Convert.ToChar(lettreActuelle + 1);
                    lettreActuelle = carte[ligne, colonne];
                }
                if (!(frontiere[1]))
                {
                    carte[ligne, colonne] = carte[ligne, colonne - 1];
                }
                if (!(frontiere[0]))
                {
                    carte[ligne, colonne] = carte[ligne - 1, colonne];
                }
            }
        }

        /*
        DecoderTrame
        Role
            decode la trame reçue 
        Paramètres
           trame : Tableau de d'entier (trame reçu du serveur)
           carte : Tableau de caractère indiquant les parcelles Foret/Mer/Terre
       Valeur de retour
           frontiere : tableau donnant les frontières d'une valeur
       */
        public static void DecoderTrame(int[,] trame, char[,] carte)
        {
            int lettreActuelle = 96;
            for (int ligne = 0; ligne < 10; ligne++)
            {
                for (int colonne = 0; colonne < 10; colonne++)
                {
                    carte[ligne, colonne] = DecoderTerrain(trame[ligne, colonne]);
                    bool[] tabfrontiere = new bool[4];
                    DecoderFrontiere(trame[ligne, colonne], ref tabfrontiere);
                    DecoderParcelle(ref lettreActuelle, carte, ligne, colonne, tabfrontiere, trame);
                }
            }
        }



    }
}
