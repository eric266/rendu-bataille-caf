﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe
{
    public class Coordonnees
    {
        private int X;
        private int Y;

        /*
        Coordonnees
         Role
             constructeur de classe
         Paramètres
            X : coordonnées X
            Y : coordonnées Y
        */
        public Coordonnees(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }

        /*
        Coordonnees
         Role
             constructeur par recopie
         Paramètres
            autreCoordonnees : coordonnées(X,Y) 
        */
        public Coordonnees(Coordonnees autreCoordonnees)
        {
            if (autreCoordonnees is null)
            {
                this.X = -1;
                this.Y = -1;
            }
            else
            {
                this.X = autreCoordonnees.X;
                this.Y = autreCoordonnees.Y;
            }
        }

        /*
        getX
         Role
             récupérer la coordonnée X de la coordonnées(X,Y)
        Paramètres
           
       Valeur de retour
            coordonnée X de la coordonnées(X,Y)
       */
        public int getX()
        {
            return this.X;
        }

        /*
        getY
         Role
             récupérer la coordonnée Y de la coordonnées(X,Y)
        Paramètres
           
       Valeur de retour
            coordonnée Y de la coordonnées(X,Y)
       */
        public int getY()
        {
            return this.Y;
        }

        /*
        VerificationVoisin
         Role
             verifi si une case à un voisin
        Paramètres
           autreCoordonnees : coordonnées(X,Y)
        Valeur de retour
            voisin : true si la case possède un voisin false sinon
       */
        public Boolean VerificationVoisin(Coordonnees autreCoordonnees)
        {
            Boolean voisin = false;
            if (this.X == autreCoordonnees.X + 1 || this.X == autreCoordonnees.X - 1) voisin = true;
            if (this.Y == autreCoordonnees.Y + 1 || this.Y == autreCoordonnees.Y - 1) voisin = true;
            return voisin;
        }

        //surcharge operateur ==
        public static bool operator ==(Coordonnees coord1, Coordonnees coord2)
        {
            if (coord1 is null || coord2 is null) return false;
            return coord1.X == coord2.X && coord1.Y == coord2.Y;
        }

        //surcharge operateur !=
        public static bool operator !=(Coordonnees coord1, Coordonnees coord2)
        {
            return !(coord1 == coord2);
        }

        //surcharge operateur =
        public override bool Equals(object obj)
        {
            return obj is Coordonnees && this == (Coordonnees)obj;
        }

    }
}
