﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe
{
    public class Jeu
    {
        private Ile ilot;
        private Joueur blanc;
        private Joueur noir;
        private IA ia;

        /*
        Jeu
        Role
           Constructeur de la classe Jeu
        Paramètres
          ilot carte du jeu
       valeur de retour

       */
        public Jeu(Ile ilot)
        {
            this.ilot = ilot;
            this.blanc = new Joueur("Blanc");
            this.noir = new Joueur("Noir");
            this.ia = new IA();
        }

        /*
        Jeu
        Role
            Constructeur de la classe Jeu
        Paramètres
            touteParcelle les parcelle de l'ile
        valeur de retour

      */
        public Jeu(List<Parcelle> touteParcelle) : this(new Ile(touteParcelle)) 
        { }

        /*
        getIlot
        Role
            getter de l'ilot
        Paramètres
           
        valeur de retour

        */
        public Ile getIlot()
        {
            return this.ilot;
        }

        /*
       getBlanc
       Role
           getter du joueur blanc
       Paramètres

       valeur de retour

       */
        public Joueur getBlanc()
        {
            return this.blanc;
        }

        /*
        getBlanc
        Role
           getter du joueur noir
        Paramètres

        valeur de retour

        */
        public Joueur getNoir()
        {
            return this.noir;
        }

        /*
        getBlanc
        Role
           getter de l'ia
        Paramètres

        valeur de retour

        */
        public IA getIa()
        {
            return this.ia;
        }

        /*
        MajJeu
        Role
           met à jour l'ilot
        Paramètres
            coordonneeNouvelleGraine cordoonée de la graine
            joueur couleur de la graine du joueur
        valeur de retour

        */
        public void MajJeu(Coordonnees coordonneeNouvelleGraine, string joueur)
        {
            ilot.getUniteeA(coordonneeNouvelleGraine).PlanterGraine(joueur);

            if (joueur == "Noire")
                noir.RetirerGraine();
            if (joueur == "Blanche")
                blanc.RetirerGraine();

            ia.setDernierreGraineCoordonnees(this.ilot.getUniteeA(coordonneeNouvelleGraine));
        }

    }
}
