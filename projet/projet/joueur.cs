﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BatailleCafe
{
    public class Joueur
    {
        private String nom;
        private int points;
        private int grainesRestantes;

        /*
        Joueur
        Role
            constructeur de classe
        paramètre
            nom: nom du joueur
        valeur de retour

        */
        public Joueur(String nom)
        {
            this.nom = (nom is null) ? "" : nom;
            this.points = 0;
            this.grainesRestantes = 28;
        }

        /*
        getPoints
        Role
            getter de l'attribut point de la classe joueur
        paramètre
            
        valeur de retour
        les points du joueurs
        */
        public int getPoints()
        {
            return points;
        }

        /*
        setPoints
        Role
            setter de l'attribut point de la classe joueur
        paramètre
            
        valeur de retour

        */
        public void setPoints(int points)
        {
            this.points = points;
        }

        /*
        getNom
        Role
            getter de l'attribut nom de la classe joueur
        paramètre
            
        valeur de retour
        nom du joueur
        */
        public String getNom()
        {
            return nom;
        }

        /*
        getGrainesRestantes
        Role
            getter de l'attribut grainesRestantes de la classe joueur
        paramètre
            
        valeur de retour
        le nombre de graines restantes au joueur
        */
        public int getGrainesRestantes()
        {
            return grainesRestantes;
        }

        /*
        setGrainesRestantes
        Role
            setter de l'attribut grainesRestantes de la classe joueur
        paramètre
            
        valeur de retour
        
        */
        public void setGrainesRestantes(int grainesRestantes)
        {
            this.grainesRestantes = grainesRestantes;
        }

        /*
        RetirerGraine
        Role
            retirer une graine quand le joueur a joué
        paramètre
            
        valeur de retour
        
        */
        public void RetirerGraine()
        {
            this.grainesRestantes--;
        }

        /*
        getTouteUnite
        Role
            retourne le nombre d'unitée posséder par le joueur
        paramètre
            
        valeur de retour
            liste des unitée que le joueur posséde
        */
        public List<Unite> getTouteUnite(Ile ilot)
        {
            List<Unite> touteUnite = new List<Unite>();
            String graineATrouvee = null;
            if (nom == "Blanc")
                graineATrouvee = "Blanche";
            if (nom == "Noir")
                graineATrouvee = "Noire";
            if (graineATrouvee is null)
                return null;
            foreach (Parcelle parcelle in ilot.getTouteParcelle())
            {
                foreach (Unite unite in parcelle.getTouteUnite())
                {
                    if (unite.getGraine() == graineATrouvee)
                        touteUnite.Add(unite);
                }
            }
            return touteUnite;
        }

        /*
        getTouteParcelle
        Role
            retourne le nombre de parcelles posséder par le joueur
        paramètre
            
        valeur de retour
            liste des parcelles que le joueur posséde
        */
        public List<Parcelle> getTouteParcelle(Ile ilot)
        {
            List<Parcelle> TouteParcelle = new List<Parcelle>();
            String graineToFound = null;
            if (nom == "Blanc")
                graineToFound = "Blanche";
            if (nom == "Noir")
                graineToFound = "Noire";
            if (graineToFound is null)
                return null;
            foreach (Parcelle parcelle in ilot.getTouteParcelle())
            {
                int nbUnite = 0;
                foreach (Unite unite in parcelle.getTouteUnite())
                {
                    if (unite.getGraine() == graineToFound)
                        nbUnite++;
                }
                if (nbUnite >= (parcelle.getNombreUnite() / 2 + 1))
                {
                    TouteParcelle.Add(parcelle);
                }
            }
            return TouteParcelle;
        }


    }
}
